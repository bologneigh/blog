json.array!(@comments) do |comment|
  json.extract! comment, :id, :post_, :body
  json.url comment_url(comment, format: :json)
end
